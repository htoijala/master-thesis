#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def plot_k(infile, outfile):
    data = np.loadtxt(infile, unpack=True, skiprows=1)
    data = list(zip(data[0], data[1], data[2]))
    encuts = sorted(list(set([tup[0] for tup in data])))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Number of $\symbf{k}$-points')
    ax.set_ylabel(r'Energy $E \unitsep \si{\electronvolt}$')
    ax.grid(True)

    ax.set_xlim(10, 22)
    ax.set_ylim(-3.735, -3.72)

    for encut in encuts:
        if encut not in [400.0, 500.0, 600.0]:
            continue

        ks = np.array([int(tup[1]) for tup in data if tup[0] == encut])
        Es = np.array([tup[2] for tup in data if tup[0] == encut])

        sort_mask = np.argsort(ks)
        ks = ks[sort_mask]
        Es = Es[sort_mask]

        ax.plot(ks, Es, 'o', linestyle='-', label=r'$E_{{\text{{cut}}}} = \SI{{{:.1f}}}{{\electronvolt}}$'.format(encut))

    ax.legend()

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
infile = sys.argv[2]

plot_k(infile, outfile)
