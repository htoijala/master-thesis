#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from read_misc import *
from read_pot import *

def plot(data, outfile):
    zs = data.z_coords()
    pot_avg = np.mean(data.data, axis=(0, 1))
    pot_max = np.max(data.data, axis=(0, 1))
    pot_min = np.min(data.data, axis=(0, 1))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$z \unitsep \si{\angstrom}$')
    ax.set_ylabel(r'Potential $V(z) \unitsep \si{\electronvolt}$')

    ax.set_xlim(np.min(zs), np.max(zs))
    ax.grid(True)

    ax.plot(zs, pot_avg, label='Mean')
    ax.plot(zs, pot_max, label='Maximum')
    ax.plot(zs, pot_min, label='Minimum')

    ax.legend(loc='lower right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
folder = sys.argv[2]

E_F = get_fermi_level(os.path.join(folder, 'vasprun.xml'))
data = datafile(os.path.join(folder, 'LOCPOT'), 'LOCPOT')
data.data -=E_F

plot(data, outfile)
