#!/usr/bin/env python3

import sys
import numpy as np
from scipy.constants import physical_constants
from scipy.special import ellipk, ellipe
import matplotlib as mpl
import matplotlib.pyplot as plt

angstrom = 1e-10 # 1 angstrom in m
c = physical_constants['speed of light in vacuum'][0] # speed of light in m/s
hbar = physical_constants['Planck constant over 2 pi in eV s'][0] # reduced Planck's constant in eVs
m_e = physical_constants['electron mass energy equivalent in MeV'][0] * 1e6 / c**2 * angstrom**2 # electron mass in eV * s^2 / angstrom^2
e = physical_constants['elementary charge'][0] # elementary charge in C
k = 14.39965 # e^2 / (4 * pi * epsilon_0) in eV * angstrom

def nu(y, approx=False):
    if approx:
        return 1.0 - y**2 * (1 - 1.0/3.0 * np.log(y))

    if y <= 1.0:
        arg = (1 - y) / (1 + y) # sqrt missing due to different integral definition in SciPy
        return np.sqrt(y + 1) * (ellipe(arg) - y * ellipk(arg))

    arg = (y - 1) / (2 * y) # sqrt missing due to different integral definition in SciPy
    return -1  *np.sqrt(y / 2) * (-2 * ellipe(arg) + (y + 1) * ellipk(arg))
nu = np.vectorize(nu, excluded=['approx'])

def probability(Ez, field, work, approx=False):
    Emax = work - 1/2**0.5 * (k * field)**0.5

    if Ez >= Emax:
        return 1.0

    y = np.sqrt(k * field) / np.abs(work - Ez)
    Q = 4.0/3.0 * 1/(hbar * field) * np.sqrt(2 * m_e * np.abs(work - Ez)**3) * nu(y)
    D = (1 + np.exp(Q))**-1

    return D
probability = np.vectorize(probability, excluded=['field', 'E_F' 'work', 'approx'])

outfile = sys.argv[1]

work = 4.5
fields = [0.1, 0.2, 0.3]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color'] # assume that the default prop cycle has at least 3 colors

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.set_xlabel(r'$E_z - E_{\text{F}} \unitsep \si{\electronvolt}$')
ax.set_ylabel(r'Transmission probability $D(E_z)$')

ax.set_xlim(-5.0, work)
ax.set_ylim(1e-30, 1e2)

ax.set_yscale('log')
ax.grid(True)

Ezs = np.linspace(-5.0, work, 1001)

ax.plot([], [], 'k--', label=r'$E_z^{\text{max}}$') # common label for the maximum energies
for i in range(len(fields)):
    ax.axvline(work - np.sqrt(0.5 * k * fields[i]), linestyle='--', linewidth=2, color=colors[i])

for i in range(len(fields)):
    ax.plot(Ezs, probability(Ezs, fields[i], work), color=colors[i], linewidth=2, label=r'$F = \SI{{{:.0f}}}{{\giga\volt\per\meter}}$'.format(10 * fields[i]))

ax.legend(loc='lower right')

fig.tight_layout(pad=0.3)
plt.savefig(outfile)
plt.close()
