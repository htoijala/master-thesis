#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib.pyplot as plt
from read_misc import *
from read_pot import *
from process_potential import *

class ImageFollower(object):
    def __init__(self, follower):
        self.follower = follower

    def __call__(self, leader):
        self.follower.set_cmap(leader.get_cmap())
        self.follower.set_clim(leader.get_clim())


def plot_slice(ax, title, data, slice_ind, slice_axis='x', images=1):
    zs = data.z_coords()
    xs_orig = data.x_coords() if slice_axis == 'x' else data.y_coords()
    pot_orig = data.data
    if slice_axis == 'x':
        pot_orig = pot_orig[:, slice_ind, :]
    else:
        pot_orig = pot_orig[slice_ind, :, :]

    xs = xs_orig.copy()
    pot = pot_orig.copy()

    iteration = 1
    while images - iteration > 0:
        axis = 0 if slice_axis == 'x' else 1
        xs = np.append(xs, xs_orig + iteration *  data.poscar.lvec[axis, axis])
        pot = np.append(pot, pot_orig, axis=0)
        iteration += 1

    pot -= np.max(pot)
    pot *= -1
    pot = pot.transpose()
    pot = np.maximum(pot, np.ones_like(pot) * 1e-3) # changing the norm later does not like zero vmin

    ax.set_title(title)
    ax.set_xlabel(r'$x \unitsep \si{\angstrom}$')
    ax.set_ylabel(r'$z \unitsep \si{\angstrom}$')

    image = ax.imshow(pot, origin='lower', extent=(xs[0], xs[-1], zs[0], zs[-1]), norm=mpl.colors.LogNorm())

    vmin = np.amin(pot)
    vmax = np.amax(pot)

    return (image, vmin, vmax)

outfile = sys.argv[1]
folder = sys.argv[2::4]
axis = sys.argv[3::4]
images = [int(x) for x in sys.argv[4::4]]
title = sys.argv[5::4]

fig = plt.figure()

grid = AxesGrid(fig, 111, nrows_ncols=(2, 2), direction='column', axes_pad=0.3, aspect=False, label_mode='L', cbar_mode='single', cbar_location='right', cbar_pad=0.1)

ims = []
vmin = np.inf
vmax = -np.inf

for i in range(len(folder)):
    E_F = get_fermi_level(os.path.join(folder[i], 'vasprun.xml'))
    data = datafile(os.path.join(folder[i], 'LOCPOT'), 'LOCPOT')
    data.data -= E_F

    dip_ind = get_dipole_step_ind(os.path.join(folder[i], 'OUTCAR'))
    crop_data_z(data, data.z_coords()[dip_ind - 10])

    axis_num = 1 if axis[i] == 'x' else 0
    colors, vmin_tmp, vmax_tmp = plot_slice(grid[i], title[i], data, data.data.shape[axis_num] // 2, axis[i], images[i])

    ims.append(colors)
    vmin = min(vmin, vmin_tmp)
    vmax = max(vmax, vmax_tmp)

norm = mpl.colors.LogNorm(vmin=vmin, vmax=vmax)
for i, im in enumerate(ims):
    im.set_norm(norm)
    if i > 0:
        ims[0].callbacksSM.connect('changed', ImageFollower(im))

fig.colorbar(ims[0], grid.cbar_axes[0])

plt.savefig(outfile)
plt.close()
