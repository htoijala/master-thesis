#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def plot_encut(infile, outfile):
    data = np.loadtxt(infile, unpack=True, skiprows=1)
    data = list(zip(data[0], data[1], data[2]))
    ks = sorted(list(set([tup[1] for tup in data])))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Cutoff energy $E_{\text{cut}} \unitsep \si{\electronvolt}$')
    ax.set_ylabel(r'Energy $E \unitsep \si{\electronvolt}$')
    ax.grid(True)

    ax.set_xlim(270, 605)
    ax.set_ylim(-3.74, -3.68)

    for k_ in ks:
        k = int(k_)
        if k not in [10, 15, 20, 22]:
            continue

        encuts = np.array([tup[0] for tup in data if tup[1] == k_])
        Es = np.array([tup[2] for tup in data if tup[1] == k_])

        sort_mask = np.argsort(encuts)
        encuts = encuts[sort_mask]
        Es = Es[sort_mask]

        ax.plot(encuts, Es, 'o', linestyle='-', label=r'$N_{{k_i}} = {:d}$'.format(k))

    ax.legend()

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
infile = sys.argv[2]

plot_encut(infile, outfile)
