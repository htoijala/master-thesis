#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

image_factor = 3.599911 # e^2 / (16 * pi * epsilon_0) in eV * angstrom

@np.vectorize
def SN_barrier(x, field, E_F, work):
    if x <= 0.0:
        return 0.0

    zero_crossing = (E_F + work) / (2 * field) + np.sqrt(((E_F + work) / (2 * field))**2 - image_factor / field)

    potential = E_F + work - field * x - image_factor / x
    if x < zero_crossing:
        potential = max(potential, 0.0)

    return potential

@np.vectorize
def find_crossings(energy, field, E_F, work):
    pre = (E_F + work - energy) / (2 * field)
    root = ((E_F + work - energy) / field)**2 - 4 * image_factor / field

    if root < 0.0: # no crossings
        return None

    root = 0.5 * root**0.5
    return (pre - root, pre + root)

def find_max(field, E_F, work):
    return ((image_factor / field)**0.5, E_F + work - 2 * (image_factor * field)**0.5) # position, value

def plot_barrier(axis, field, E_F, work, xlims, label):
    x = np.linspace(xlims[0], xlims[1], 501)
    y = SN_barrier(x, field, E_F, work)

    ax.plot(x, y, linewidth=2, label=label)

    ax.plot([0.0, (E_F + work) / field], [E_F + work, 0.0], 'k--', linewidth=2)

outfile = sys.argv[1]

E_F = 7.5
work = 7.5
fields = [0.5, 1.5]
xlims=(-5.0, 40.0)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.set_xlabel(r'$z$')
ax.set_ylabel(r'Potential')

ax.set_xticks([0.0])
ax.set_xticklabels([r'Surface'])
ax.set_yticks([0.0, E_F, E_F + work])
ax.set_yticklabels([r'$0$', r'$E_{\text{F}}$', r'$E_{\text{F}} + \phi$'])

ax.set_xlim(-5, (E_F + work) / min(fields))
ax.set_ylim(-0.1, E_F + work)

for i, field in enumerate(fields):
    plot_barrier(ax, field, E_F=E_F, work=work, xlims=xlims, label='Field {:d}'.format(i+1))

ax.axvline(0.0, color='k')

ax.text(-4.5, E_F/2, 'Metal')
ax.text(0.8 * (E_F + work) / min(fields), E_F/2, 'Vacuum')

ax.legend(loc='upper right')

fig.tight_layout(pad=0.3)
plt.savefig(outfile)
plt.close()
