#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def plot_transmittances(E, T, undersamplings, outfile):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$E - E_{\text{F}} / eV$')
    ax.set_ylabel(r'Ratio of Transmittance Probabilities')

    ax.set_xlim(-1, 3)
    ax.set_ylim(0.9, 2)
    ax.grid(True)

    for i in range(len(paths)):
        ax.plot(E[i], T[i] / T[0], '.', linestyle='-', label=r'${:d} \times$ undersampling'.format(undersamplings[i]))

    ax.legend(loc='upper right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
paths = sys.argv[2::2]
undersamplings = [int(x) for x in sys.argv[3::2]]

energies = []
transmittances = []
for i in range(len(paths)):
    E, T = np.loadtxt(paths[i], skiprows=2, unpack=True)
    energies.append(E)
    transmittances.append(T)

plot_transmittances(energies, transmittances, undersamplings, outfile)
