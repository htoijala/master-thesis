#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def plot_energies(data, outfile):
    # only plot work function

    layers = data[0]
    Es = data[1]
    E_Fs = data[2]
    E_vacs = data[3]
    TSs  = data[4]

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Number of monolayers')
    ax.set_ylabel(r'Work function $\phi \unitsep \si{\electronvolt}$')

    ax.grid(True)
    ax.set_xlim(layers[0] - 1, layers[-1] + 1)

    ax.plot(layers, E_vacs - E_Fs, 'o', linestyle='-')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

def fit_energy(data):
    layers = data[0]
    Es = data[1]

    num_fit = 3
    slope, intercept = np.polyfit(2 * layers[-num_fit:], Es[-num_fit:], deg=1)
    print('Linear fit to energy:')
    print('slope:', slope)
    print('intercept:', intercept)

def plot_movements(data, outfile):
    layers = data[0]
    moves = data[5:] * 100 # in percent

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Number of monolayers')
    ax.set_ylabel(r'Monolayer spacing deviation  $\unitsep \si{\percent}$')

    ax.grid(True)

    for i in range(len(moves) - 1): # do not plot last one with only one data point
        first_ind = len(moves[i]) - 1
        while first_ind > 0 and np.isfinite(moves[i][first_ind]):
            first_ind -= 1

        ax.plot(layers[first_ind:], moves[i][first_ind:], 'o', linestyle='-', label='Layer {:d}'.format(i+2))

    ax.legend(loc='lower left')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
infile = sys.argv[2]
action = sys.argv[3]

data = np.loadtxt(infile, skiprows=1).transpose()
if action == 'work':
    plot_energies(data, outfile)
elif action == 'movement':
    plot_movements(data, outfile)
if action == 'energy-fit':
    fit_energy(data)
