#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from read_misc import *
from read_pot import *
from process_potential import *

def get_potential(folder, image=False):
    E_F = get_fermi_level(os.path.join(folder, 'vasprun.xml'))
    dip_ind = get_dipole_step_ind(os.path.join(folder, 'OUTCAR'))
    data = datafile(os.path.join(folder, 'LOCPOT'), 'LOCPOT')

    if not image:
        smooth_data(data)

    crop_data_z(data, data.z_coords()[dip_ind] - 2.0)

    if image:
        efield = get_efield(os.path.join(folder, 'vasprun.xml'))
        with open(os.path.join(folder, 'zim.txt'), 'r') as fd:
            z_im = float(fd.readline().strip())
        extend_potential_image(data, E_F - 5.0, efield, z_im)
    else:
        extend_potential(data, E_F - 5.0)

    zs = data.z_coords()
    pot = np.mean(data.data, axis=(0, 1))
    pot -= E_F

    return (zs, pot)

def plot(data_orig, data_imag, outfile):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$z \unitsep \si{\angstrom}$')
    ax.set_ylabel(r'Average potential $V(z) \unitsep \si{\electronvolt}$')

    ax.set_xlim(15.0, 50.0)
    ax.set_ylim(-2.0, 5.0)
    ax.grid(True)

    ax.plot(data_orig[0], data_orig[1], linewidth=2, label='PBE')
    ax.plot(data_imag[0], data_imag[1], linewidth=2, label='Image')

    ax.legend(loc='upper right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
folder_orig = sys.argv[2]
folder_imag = sys.argv[3]

data_orig = get_potential(folder_orig, image=False)
data_imag = get_potential(folder_imag, image=True)

plot(data_orig, data_imag, outfile)
