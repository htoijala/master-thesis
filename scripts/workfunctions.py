#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from read_misc import *
from read_pot import *

def plot_datas(datas, labels, z_lims, E_lims, outpath):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$z \unitsep \si{\angstrom}$')
    ax.set_ylabel(r'Hartree potential $V \unitsep \si{\electronvolt}$')

    ax.set_xlim(*z_lims)
    ax.set_ylim(*E_lims)
    ax.grid(True)

    for data, label in zip(datas, labels):
        ax.plot(data[0], data[1], linewidth=2, label=label)

    ax.legend(loc='lower right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outpath)
    plt.close()

outpath = sys.argv[1]
folders = sys.argv[2::2]
labels = sys.argv[3::2]

datas = []
low_z = np.inf
high_z = -np.inf
low_E = np.inf
high_E = -np.inf
for folder in folders:
    E_F = get_fermi_level(os.path.join(folder, 'vasprun.xml'))
    path = os.path.join(folder, 'LOCPOT')
    locpot = datafile(path, 'LOCPOT')
    locpot.data -= E_F

    sb_end = locpot.poscar.substrate_end()
    low_z = min(low_z, sb_end)

    dipole_ind = get_dipole_step_ind(os.path.join(folder, 'OUTCAR'))
    dipole_z = locpot.z_coords()[dipole_ind]
    high_z = max(high_z, dipole_z - 5.0)

    work = get_vacuum_level(os.path.join(folder, 'OUTCAR')) - E_F
    low_E = min(low_E, work - 6.0)
    high_E = max(high_E, work + 0.5)

    z = locpot.z_coords()
    pot = np.mean(locpot.data, axis=(0, 1))

    datas.append([z, pot])

plot_datas(datas, labels, (low_z, high_z), (low_E, high_E), outpath)
