#!/usr/bin/env python3

import bisect
import sys
import numpy as np
import scipy.integrate as spi
import matplotlib as mpl
import matplotlib.pyplot as plt
from FN_current import total_current

def get_FN_current(field, work):
    T = 0.0
    ret = total_current(W=work, F=field, T=T)
    return ret

def get_temps(path):
    temps = []
    with open(path, 'r') as fd:
        line = fd.readline()
        line = fd.readline()
        line = fd.readline()

        tokens = line.split()

        for token in tokens:
            if token.startswith('T=') and token.endswith('eV'):
                temp = float(token[2:-2])
                temps.append(temp)

    return temps

def integrate_current(E, I):
    return spi.simps(I, E)

def read_arg_file(path):
    with open(path, 'r') as fd:
        lines = fd.readlines()

    data = {}

    linenum = 0
    status = 'label'

    label = ''
    for line in lines:
        linenum += 1
        L = line.strip()
        if L.startswith('#') or len(L) == 0:
            continue

        if L.startswith('['):
            status = 'label'

        if status == 'label':
            if not L.startswith('[') or not L.endswith(']'):
                raise RuntimeError('Malformed label on line {:d}.'.format(linenum))

            label = L[1:-1]
            data[label] = {}
            data[label]['data'] = {}
            status = 'work'

            continue

        if status == 'work':
            try:
                work = float(L)
            except ValueError:
                raise RuntimeError('Malformed work function on line {:d}.'.format(linenum))

            data[label]['work'] = work
            status = 'nu_params'

            continue

        if status == 'nu_params':
            try:
                nu_params = [float(t) for t in L.split()]
            except ValueError:
                raise RuntimeError('Malformed nu params on line {:d}.'.format(linenum))

            data[label]['nu_params'] = nu_params
            status = 'files'

            continue

        if status == 'files':
            ind = L.find(' ')
            if ind == -1:
                raise RuntimeError('No space on line {:d}.'.format(linenum))

            try:
                field = float(L[:ind])
            except ValueError:
                raise RuntimeError('Malformed field strength on line {:d}.'.format(linenum))

            filepath = L[ind:].strip()
            if filepath.startswith('"') and filepath.endswith('"'):
                filepath = filepath[1:-1]

            try:
                Ts = get_temps(filepath)
                curr_data = np.loadtxt(filepath, skiprows=3, unpack=False).transpose()
            except IOError:
                raise RuntimeError('Unable to read file at {}.'.format(filepath))

            Es = curr_data[0]
            dIs = curr_data[1:]
            Is = [integrate_current(Es, dI) for dI in dIs]

            for T, I in zip(Ts, Is):
                if T not in data[label]['data'].keys():
                    data[label]['data'][T] = [[], []]

                ind = bisect.bisect_left(data[label]['data'][T][0], field)
                data[label]['data'][T][0].insert(ind, field)
                data[label]['data'][T][1].insert(ind, I)

            continue

    for L in data.keys():
        for T in data[L]['data'].keys():
            data[L]['data'][T] = np.array(data[L]['data'][T])

    return data

def rescale(F, I, work, nu_0):
    a = 1/(16*np.pi) / 1.6021766e-19 # e/(16*pi*hbar) in electrons / (eV * s)
    b = 4/3 * 0.2624684**0.5         # 4/(3*e) * (2 * m_e / hbar^2)**0.5 in 1/(electrons * angstrom * eV**0.5)

    x = nu_0 * b * work**1.5 / F
    y = I / F**2 * work / a

    return (x, y)

def plot(data, outfile):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$\nu_0 b \phi^{3/2} / F$')
    ax.set_ylabel(r'$\phi J / (a F^2)$')

    ax.grid(True)
    ax.set_yscale('log', basey=np.e)

    def ticks(y, pos):
        return r'$\symup{{e}}^{{{:.0f}}}$'.format(np.log(y))
    ax.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(ticks))

    for L in sorted(data.keys()):
        work = data[L]['work']
        nu_0 = data[L]['nu_params'][-1]
        for T in sorted(data[L]['data'].keys()):
            if T > 0.0: # at the moment only plot zero temperature data
                continue

            x, y = rescale(data[L]['data'][T][0], data[L]['data'][T][1], work, nu_0)

            ax.plot(x, y, marker='o', label='{}'.format(L))

    FN_fields = np.array([0.1, 0.2, 0.3])
    FN_work = data['Clean']['work']
    FN_currents = np.array([get_FN_current(F, FN_work) for F in FN_fields])
    x, y = rescale(FN_fields, FN_currents, FN_work, 1.0)
    ax.plot(x, y, label='Schottky--Nordheim')

    ax.legend(loc='upper right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
infile = sys.argv[2]

data = read_arg_file(infile)
plot(data, outfile)
