#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

def plot_data(infile, outfile):
    lconsts, Es, TSs = np.loadtxt(infile, unpack=True, skiprows=1)
    mask = np.argsort(lconsts)
    lconsts = lconsts[mask]
    Es = Es[mask]
    TSs = TSs[mask]

    mask = np.logical_and(lconsts >= 3.60, lconsts <= 3.67)
    lconsts = lconsts[mask]
    Es = Es[mask]
    TSs = TSs[mask]

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Lattice constant $\unitsep \si{\angstrom}$')
    ax.set_ylabel(r'Energy $E \unitsep \si{\electronvolt}$')
    ax.grid(True)

    ax.set_xlim(3.60, 3.67)
    ax.set_ylim(-3.73, -3.72)

    ax.plot(lconsts, Es, '.', linestyle='-')

    ax_inset = inset_axes(ax, width='50%', height='50%', loc='upper center')
    ax_inset.grid(True)

    ax_inset.set_xlim(3.63, 3.64)
    ax_inset.set_ylim(-3.72865, -3.72850)

    ax_inset.set_xticks([3.632, 3.634, 3.636, 3.638])
    ax_inset.set_yticks([-3.72865, -3.72860, -3.72855, -3.72850])
    def ticks(y, pos):
        return r'${:.5f}$'.format(y)
    ax_inset.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(ticks))

    mask = np.logical_and(lconsts >= 3.63, lconsts <= 3.64)
    ax_inset.plot(lconsts[mask], Es[mask], 'o', linestyle='-')
    mark_inset(ax, ax_inset, loc1=3, loc2=4, fc="none", ec="0.5")

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
infile = sys.argv[2]

plot_data(infile, outfile)
