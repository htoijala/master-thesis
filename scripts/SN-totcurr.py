#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from FN_current import *

outfile = sys.argv[1]

W = 4.76
Ts = [0.0, 300.0 * kB, 800.0 * kB, 1300.0 * kB]
Fs = np.logspace(-2, 1, num=201, base=10.0)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.set_xlabel(r'Electric field $F \unitsep \si{\giga\volt\per\meter}$')
ax.set_ylabel(r'Current density $J \unitsep \si{\ampere\per\square\meter}$')

ax.set_xlim(1e-2 * 10, 1e1 * 10) # * 10 due to unit conversion to GV/m
ax.set_ylim(1e-73, 1e20)

ax.set_xscale('log')
ax.set_yscale('log')
ax.grid(True)

for T in Ts:
    currs = [e / angstrom**2 * total_current(W, F, T) for F in Fs]
    ax.plot(10 * Fs, currs, linewidth=2, label=r'$T = \SI{{{:.0f}}}{{\kelvin}}$'.format(T / kB))

ax.legend()

plt.tight_layout(pad=0.3)
plt.savefig(outfile)
plt.close()
