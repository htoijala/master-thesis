#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from read_misc import *
from read_pot import *
from process_potential import *

def scale_axes(axis, x_scale, y_scale, z_scale):
    scale=np.diag([x_scale, y_scale, z_scale, 1.0])
    scale=scale*(1.0/scale.max())
    scale[3,3]=1.0

    def short_proj():
        return np.dot(Axes3D.get_proj(axis), scale)

    axis.get_proj = short_proj

def plot_slice(outfile, data, slice_ind, slice_axis='x', images=1):
    zs = data.z_coords()
    xs_orig = data.x_coords() if slice_axis == 'x' else data.y_coords()
    pot_orig = data.data
    if slice_axis == 'x':
        pot_orig = pot_orig[:, slice_ind, :]
    else:
        pot_orig = pot_orig[slice_ind, :, :]

    xs = xs_orig.copy()
    pot = pot_orig.copy()
    pot[pot < 0.0] = 0.0

    iteration = 1
    while images - iteration > 0:
        axis = 0 if slice_axis == 'x' else 1
        xs = np.append(xs, xs_orig + iteration *  data.poscar.lvec[axis, axis])
        pot = np.append(pot, pot_orig, axis=0)
        iteration += 1

    pot = pot.transpose()

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')

    #ax.set_xlabel(r'$x / \si{\angstrom}$')
    #ax.set_ylabel(r'$z / \si{\angstrom}$')
    #ax.set_zlabel(r'$V / \si{\electronvolt}$')

    ax.set_xlim(xs[0], xs[-1])
    ax.set_xticks([1.0 * i for i in range(int(xs[-1]) + 1) if i % 5 == 0])

    ax.set_ylim(zs[0], zs[-1])
    ax.set_yticks([1.0 * i for i in range(int(zs[-1]) + 1) if i % 5 == 0])

    ax.set_zlim(0.0, np.ceil(np.max(pot) + 0.7))
    ax.set_zticks([i for i in range(100) if i <= ax.get_zlim()[-1]])

    X, Z = np.meshgrid(xs, zs)
    colors = ax.plot_surface(X, Z, pot, cmap=mpl.cm.viridis, cstride=1, rstride=1, linewidth=0)
    scale_axes(ax, 1.0, 1.0, 1.2)
    ax.view_init(50, -40)

    size = fig.get_size_inches()
    size[1] = 2.15
    fig.set_size_inches(*size)
    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
folder = sys.argv[2]
axis = sys.argv[3]
images = int(sys.argv[4])

thin_factor = 5

efield = get_efield(os.path.join(folder, 'vasprun.xml'))
with open(os.path.join(folder, 'zim.txt'), 'r') as fd:
    zim = float(fd.readline().strip())

E_F = get_fermi_level(os.path.join(folder, 'vasprun.xml'))
data = datafile(os.path.join(folder, 'LOCPOT'), 'LOCPOT')
data.data -= E_F

crop_z = data.z_coords()[get_dipole_step_ind(os.path.join(folder, 'OUTCAR'))] - 3.0
crop_data_z(data, crop_z)
crop_data_substrate(data, leave=2.0)

extend_potential_image(data, 0.0, efield, zim)

crop_data_z(data, 22.0)

thin_data(data, thin_factor, thin_factor, thin_factor)

axis_num = 1 if axis == 'x' else 0
adatom_pos = data.poscar.atom_coords[-1, axis_num]
if axis == 'x':
    adatom_ind = np.searchsorted(data.y_coords(), adatom_pos, side='left')
else:
    adatom_ind = np.searchsorted(data.x_coords(), adatom_pos, side='left')

plot_slice(outfile, data, adatom_ind, axis, images)
