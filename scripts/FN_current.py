#!/usr/bin/env python3

import numpy as np
from scipy.constants import physical_constants
from scipy.integrate import quad
from scipy.special import ellipk, ellipe

angstrom = 1e-10 # 1 angstrom in m
c = physical_constants['speed of light in vacuum'][0] # speed of light in m/s
hbar = physical_constants['Planck constant over 2 pi in eV s'][0] # reduced Planck's constant in eVs
m_e = physical_constants['electron mass energy equivalent in MeV'][0] * 1e6 / c**2 * angstrom**2 # electron mass in eV * s^2 / angstrom^2
e = physical_constants['elementary charge'][0] # elementary charge in C
k = 14.39965 # e^2 / (4 * pi * epsilon_0) in eV * angstrom
kB = physical_constants['Boltzmann constant in eV/K'][0] # Boltzmann constant in eV/K

def nu(y, approx=False):
    if approx:
        return 1.0 - y**2 * (1 - 1.0/3.0 * np.log(y))

    if y <= 1.0:
        arg = (1 - y) / (1 + y) # sqrt missing due to different integral definition in SciPy
        return np.sqrt(y + 1) * (ellipe(arg) - y * ellipk(arg))

    arg = (y - 1) / (2 * y) # sqrt missing due to different integral definition in SciPy
    return -1  *np.sqrt(y / 2) * (-2 * ellipe(arg) + (y + 1) * ellipk(arg))
nu = np.vectorize(nu, excluded=['approx'])

def probability(Ez, field, work, approx=False):
    Emax = work - 1/2**0.5 * (k * field)**0.5

    if Ez >= Emax:
        return 1.0

    y = np.sqrt(k * field) / np.abs(work - Ez)
    Q = 4.0/3.0 * 1/(hbar * field) * np.sqrt(2 * m_e * np.abs(work - Ez)**3) * nu(y)

    if Q > 700.0:
        return 0.0

    D = (1 + np.exp(Q))**-1

    return D
probability = np.vectorize(probability, excluded=['field', 'E_F' 'work', 'approx'])

def supply_function(Ez, T):
    factor = m_e / (2  *np.pi**2 * hbar**3) # supply prefactor in 1/(eV^2 * s * angstrom^2)

    if T == 0:
        return max(factor * (-Ez), 0.0)

    exponent = -Ez / T
    if exponent > 500:
        return factor * (-Ez)

    exponential = np.exp(exponent)
    if exponential > 1e-12:
        logarithm = np.log(1.0 + exponential)
    else:
        logarithm = exponential - exponential**2 / 2

    return factor * T * logarithm
supply_function = np.vectorize(supply_function, excluded=['T'])

def differential_current(Ez, W, F, T):
    ret = supply_function(Ez, T) * probability(Ez, F, W)
    return ret
differential_current = np.vectorize(differential_current, excluded=['W', 'F', 'T'])

def total_current(W, F, T):
    integrand = lambda Ez: supply_function(Ez, T) * probability(Ez, F, W)
    integral =  quad(integrand, -np.inf, np.inf, epsabs=0.0, epsrel=1e-3)
    return integral[0]
