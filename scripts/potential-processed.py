#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from read_doscar import *
from read_misc import *
from read_pot import *
from process_potential import *

def plot(data, E_F, outfile):
    zs = data.z_coords()
    pot_avg = np.mean(data.data, axis=(0, 1))
    pot_max = np.max(data.data, axis=(0, 1))
    pot_min = np.min(data.data, axis=(0, 1))

    ind = 0
    while pot_max[ind] < E_F:
        ind += 1

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$z \unitsep \si{\angstrom}$')
    ax.set_ylabel(r'Potential $V(z) \unitsep \si{\electronvolt}$')

    ax.set_xlim(np.min(zs), np.max(zs))
    ax.set_ylim(-1, 16)
    ax.grid(True)

    ax.plot(zs, pot_avg, label='Mean')
    ax.plot(zs, pot_max, label='Maximum')
    ax.plot(zs, pot_min, label='Minimum')

    ax.plot([0.0, zs[ind]], [E_F, E_F], linewidth=2, label=r'$E_{\text{F}}$')

    ax.legend(loc='upper right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
folder = sys.argv[2]

dipole_ind = get_dipole_step_ind(os.path.join(folder, 'OUTCAR'))
E_F_calc = get_fermi_level(os.path.join(folder, 'vasprun.xml'))
E_F = -1 * DOSCAR(os.path.join(folder, 'DOSCAR'), normalize_to_E_F=True).lowest_state_energy()
data = datafile(os.path.join(folder, 'LOCPOT'), 'LOCPOT')

smooth_data(data)
crop_data_z(data, data.z_coords()[dipole_ind] - 2.0)
crop_data_substrate(data)
extend_potential(data, E_F_calc - E_F)
thin_data(data, 12, 12, 12)

data.data -= (E_F_calc - E_F)
mask = get_clamp_mask(data, 0.0)
data.data[mask] = 0.0

plot(data, E_F, outfile)
