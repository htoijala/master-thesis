#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

@np.vectorize
def transition_factor(x):
    if x < 0:
        return 1.0

    smooth_length = 0.75
    factor = np.exp(-x / smooth_length)
    ret = factor * 1 + (1 - factor) * np.exp(-x)
    return ret

def plot(outfile):
    x = np.linspace(-1.0, 5.0, 501)
    y = transition_factor(x)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'Transition function')

    ax.set_xlim(-1, 5)
    ax.set_ylim(0.0, 1.2)
    ax.grid(True)

    ax.plot(x, y, linewidth=2)

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]

plot(outfile)
