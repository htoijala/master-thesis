#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from read_misc import *
from read_pot import *
from process_potential import *
from charge_com import get_excess_charge_COM

def plot_xc(locpot_har, locpot_tot, z_im, dip_ind, outfile):
    image_factor = 3.599911 # factor for the image potential in eV * angstrom

    def transition_factor(x):
        # Function which fulfills the following:
        #     1. = 1.0 for x < 0.0
        #     2. decays to zero with a characteristic scale of 1.0
        #     3. smooth everywhere

        if x < 0:
            return 1.0

        smooth_length = 0.75
        factor = np.exp(-x / smooth_length)
        return factor * 1 + (1 - factor) * np.exp(-x)

    def closest_atom_distance(x, y, z, atoms, cell_size):
        coords = np.array([x, y, z])

        diffs = np.abs(atoms - coords)
        diffs_x = diffs[:, 0]
        diffs_y = diffs[:, 1]
        diffs_z = diffs[:, 2]

        # take periodic boundaries in x and y directions into account
        mask_x = diffs_x > cell_size[0] / 2.0
        mask_y = diffs_y > cell_size[1] / 2.0
        diffs_x[mask_x] = cell_size[0] - diffs_x[mask_x]
        diffs_y[mask_y] = cell_size[1] - diffs_y[mask_y]

        sq_diffs = diffs_x**2 + diffs_y**2 + diffs_z**2
        return np.min(sq_diffs)**0.5

    def xc_potential(xc_dft, xs, ys, zs, atoms, cell_size, z_im, sb_end):
        trans_start = 1.5  # start transition to image potential this far + z_im away from the nearest atom (angstrom)
        trans_length = 1.0 # the characteristic length of the transition in angstrom

        ret = np.empty_like(xc_dft)

        atom_max_z = np.max(atoms[:, 2])
        for k in range(len(zs)): # this triple loop is slow
            z = zs[k]

            if z < sb_end + z_im + trans_start: # no image potential in the slab
                ret[:, :, k] = xc_dft[:, :, k]
                continue

            image_pot = -1 * image_factor / (z - (sb_end + z_im))

            if z > atom_max_z + z_im + trans_start + 10 * trans_length: # optimization, XC factor must be of order <= 1e-5 here
                ret[:, :, k] = image_pot
                continue

            for j in range(len(ys)):
                y = ys[j]
                for i in range(len(xs)):
                    dist = closest_atom_distance(xs[i], y, z, atoms, cell_size)
                    dist = min(dist, z - sb_end) - z_im
                    factor = transition_factor((dist - trans_start) / trans_length)
                    ret[i, j, k] = factor * xc_dft[i, j, k] + (1 - factor) * image_pot
        return ret

    locpot_xc = locpot_tot.copy()
    locpot_xc.data -= locpot_har.data

    # hartree potential is always smooth, no need to smooth locpot_har
    smooth_data(locpot_xc)

    zs = locpot_har.z_coords()[:dip_ind]
    pot_har = locpot_har.data[:,:,:dip_ind]
    pot_tot = locpot_tot.data[:,:,:dip_ind]
    pot_xc = locpot_xc.data[:,:,:dip_ind]

    sb_end = locpot_har.poscar.substrate_end()
    cell_size = [locpot_xc.poscar.lvec[i, i] for i in range(3)]

    pot_result = xc_potential(pot_xc, locpot_xc.x_coords(), locpot_xc.y_coords(), zs, locpot_xc.poscar.atom_coords, cell_size, z_im, sb_end)

    line_pbe = np.mean(pot_xc, axis=(0, 1))
    line_result = np.mean(pot_result, axis=(0, 1))

    line_image = -1 * image_factor / (zs - (sb_end + z_im))
    line_image[np.logical_or(line_image > 0, line_image < np.min(line_pbe) * 1.2)] = np.nan

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'$z \unitsep \si{\angstrom}$')
    ax.set_ylabel(r'$|V_{\text{xc}}| \unitsep \si{\electronvolt}$')

    ax.set_yscale('log')
    ax.grid(True)

    ax.plot(zs, np.abs(line_image), 'C0-', label='Image')
    ax.plot(zs, np.abs(line_pbe), 'C1-', label='PBE')
    ax.plot(zs, np.abs(line_result), 'C2--', label='Result')

    ax.legend(loc='lower left')

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
folder_nf = sys.argv[2]
folder_har = sys.argv[3]
folder_tot = sys.argv[4]

locpot_har = datafile(os.path.join(folder_har, 'LOCPOT'), 'LOCPOT')
locpot_tot = datafile(os.path.join(folder_tot, 'LOCPOT'), 'LOCPOT')

# Make the Fermi levels equal.
# They appear to always be equal anyway, but do this for safety.
# This means that the supplementary files (CONTCAR, DOSCAR, OUTCAR, vasprun.xml)
# should be taken from the field_x.x-har folder.
E_F_har = get_fermi_level(os.path.join(folder_har, 'vasprun.xml'))
E_F_tot = get_fermi_level(os.path.join(folder_tot, 'vasprun.xml'))
locpot_tot.data += E_F_har - E_F_tot

dip_ind_har = get_dipole_step_ind(os.path.join(folder_har, 'OUTCAR'))
dip_ind_tot = get_dipole_step_ind(os.path.join(folder_tot, 'OUTCAR'))
dip_ind = min(dip_ind_har, dip_ind_tot)

left_COM, right_COM = get_excess_charge_COM(folder_nf, folder_tot)
z_im = right_COM - locpot_tot.poscar.substrate_end()

plot_xc(locpot_har, locpot_tot, z_im, dip_ind, outfile)
