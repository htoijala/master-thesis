#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def plot_data(infile, reference_energy, outfile):
    sigmas, Es, TSs = np.loadtxt(infile, unpack=True, skiprows=1)
    mask = np.argsort(sigmas)
    sigmas = sigmas[mask]
    Es = Es[mask]
    TSs = TSs[mask]

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Smearing parameter $\sigma \unitsep \si{\electronvolt}$')
    ax.set_ylabel(r'Energy (Entropy) $\unitsep \si{\electronvolt}$')
    ax.grid(True)

    ax.plot(sigmas, Es - reference_energy, 'o', linestyle='-', label='$\symup{\Delta}E$')
    ax.plot(sigmas, TSs, 'o', linestyle='-', label='$TS$')

    ax.legend()

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
infile = sys.argv[2]
ref_E = -3.72809445 # taken from 600 eV, 20 kpts with tetrahedron method

plot_data(infile, ref_E, outfile)
