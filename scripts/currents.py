#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def get_temps(path):
    temps = []
    with open(path, 'r') as fd:
        line = fd.readline()
        line = fd.readline()
        line = fd.readline()

        tokens = line.split()

        for token in tokens:
            if token.startswith('T=') and token.endswith('eV'):
                temp = float(token[2:-2])
                temps.append(temp)

    return temps

def read_arg_file(path):
    with open(path, 'r') as fd:
        lines = fd.readlines()

    data = {}

    linenum = 0
    status = 'label'

    prop_cycle = plt.rcParams['axes.prop_cycle']
    plot_params = [parset for parset in prop_cycle]

    label = ''
    for line in lines:
        linenum += 1
        L = line.strip()
        if L.startswith('#') or len(L) == 0:
            continue

        if L.startswith('['):
            status = 'label'

        if status == 'label':
            if not L.startswith('[') or not L.endswith(']'):
                raise RuntimeError('Malformed label on line {:d}.'.format(linenum))

            label = L[1:-1]
            data[label] = {}
            data[label]['plot_params'] = plot_params[(len(data.keys()) - 1) % len(plot_params)]

            status = 'files'
            continue

        if status == 'files':
            ind = L.find(' ')
            if ind == -1:
                raise RuntimeError('No space on line {:d}.'.format(linenum))

            try:
                field = float(L[:ind])
            except ValueError:
                raise RuntimeError('Malformed field strength on line {:d}.'.format(linenum))

            filepath = L[ind:].strip()
            if filepath.startswith('"') and filepath.endswith('"'):
                filepath = filepath[1:-1]

            try:
                Ts = get_temps(filepath)
                curr_data = np.loadtxt(filepath, skiprows=3, unpack=False).transpose()
            except IOError:
                raise RuntimeError('Unable to read file at {}.'.format(filepath))

            E = curr_data[0]
            dIs = curr_data[1:]

            if not 'data' in data[label].keys():
                data[label]['data'] = {}

            if not field in data[label]['data'].keys():
                data[label]['data'][field] = {}

            for i, T in enumerate(Ts):
                if T not in [0.0, 0.025]:
                    continue

                if T not in data[label]['data'][field].keys():
                    data[label]['data'][field][T] = (E, dIs[i])

            continue

    return data

def plot(data, outpath):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Normal energy $E_{z} - E_{\text{F}} \unitsep \si{\electronvolt}$')
    ax.set_ylabel(r'Differential current density $j(E_z) \unitsep \si{\electron\per\electronvolt\per\second\per\square\angstrom}$')

    ax.set_xlim(-3, 5)
    ax.grid(True)
    ax.set_yscale('log')

    in_legend = {}

    for L in sorted(data.keys()):
        in_legend[L] = False

        for field in sorted(data[L]['data'].keys()):
            for T in sorted(data[L]['data'][field].keys()):
                if T == 0.0 and field == 0.1:
                    E, dI = data[L]['data'][field][T]
                    ax.plot(E, dI, linestyle='--', linewidth=2, **data[L]['plot_params']) # assume that the default prop_cycle does not use linestyles

                if T != 0.025:
                    continue

                E, dI = data[L]['data'][field][T]
                ax.plot(E, dI, label=(L if not in_legend[L] else None), linewidth=2, **data[L]['plot_params'])
                in_legend[L] = True

    ax.text(-1.8, 2e-40, r'\SI{1}{\giga\volt\per\meter}')
    ax.text(-2.0, 1e-16, r'\SI{2}{\giga\volt\per\meter}')
    ax.text(0.7, 2e-4, r'\SI{3}{\giga\volt\per\meter}')

    ax.legend(loc='upper right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outpath)
    plt.close()

outpath = sys.argv[1]
inpath = sys.argv[2]

data = read_arg_file(inpath)
plot(data, outpath)
