#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from read_misc import *
from read_pot import *

def plot_slice(outfile, data, slice_ind, slice_axis='x', images=1):
    zs = data.z_coords()
    xs_orig = data.x_coords() if slice_axis == 'x' else data.y_coords()
    pot_orig = data.data
    if slice_axis == 'x':
        pot_orig = pot_orig[:, slice_ind, :]
    else:
        pot_orig = pot_orig[slice_ind, :, :]

    xs = xs_orig.copy()
    pot = pot_orig.copy()
    
    iteration = 1
    while images - iteration > 0:
        axis = 0 if slice_axis == 'x' else 1
        xs = np.append(xs, xs_orig + iteration *  data.poscar.lvec[axis, axis])
        pot = np.append(pot, pot_orig, axis=0)
        iteration += 1

    pot -= np.max(pot)
    pot *= -1
    pot = pot.transpose()

    fig = plt.figure()
    ax = fig.add_subplot(1, 2, 2)

    ax.set_xlabel(r'$x \unitsep \si{\angstrom}$')
    ax.set_ylabel(r'$z \unitsep \si{\angstrom}$')

    colors = ax.imshow(pot, origin='lower', extent=(xs[0], xs[-1], zs[0], zs[-1]), norm=mpl.colors.LogNorm())
    fig.colorbar(colors)

    ax2 = fig.add_subplot(1, 2, 1)

    ax2.set_xlabel(r'$z / \si{\angstrom}$')
    ax2.set_ylabel(r'Mean potential $V(z) / \si{\electronvolt}$')

    ax2.grid(True)

    ax2.plot(data.z_coords(), np.mean(data.data, axis=(0, 1)))

    fig.tight_layout(pad=0.3)
    plt.savefig(outfile)
    plt.close()

outfile = sys.argv[1]
folder = sys.argv[2]
axis = sys.argv[3]
images = int(sys.argv[4])

E_F = get_fermi_level(os.path.join(folder, 'vasprun.xml'))
data = datafile(os.path.join(folder, 'LOCPOT'), 'LOCPOT')
data.data -= E_F

axis_num = 1 if axis == 'x' else 0
plot_slice(outfile, data, data.data.shape[axis_num] // 2, axis, images)
