#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from read_doscar import *
from read_poscar import *

@np.vectorize
def free_dos(E, E_F):
    factor = 1/(2*np.pi**2) * 0.2624684**1.5 # number is 2 * m_e / hbar^2 in 1/(eV * angstrom^2)

    if E < -E_F:
        return 0.0

    return factor * np.sqrt(E + E_F)

def plot_dos(dos, E_F, E_F_free, outpath):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Energy $E - E_{\text{F}} \unitsep \si{\electronvolt}$')
    ax.set_ylabel(r'Density of states \unitsep \si{\per\electronvolt\per\cubic\angstrom}$')

    ax.set_xlim(np.min(dos.energy), np.max(dos.energy))
    ax.set_ylim(0.0, np.max(dos.pdos))
    ax.grid(True)

    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    ax.fill_between(dos.energy, dos.pdos, 0, where=(dos.energy <= E_F), color=colors[0], alpha=0.5)
    ax.plot(dos.energy, dos.pdos, color=colors[0], label='Data')

    fdos = free_dos(dos.energy, E_F_free)
    mask = np.logical_and(fdos > 0.0, dos.energy <= E_F + 1.0)
    ax.plot(dos.energy[mask], fdos[mask], color=colors[1], label='Free electrons')

    ax.legend(loc='upper right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outpath)
    plt.close()

outpath = sys.argv[1]
doscar_path = sys.argv[2]
poscar_path = sys.argv[3]

pos = POSCAR(poscar_path)
dos = DOSCAR(doscar_path, normalize_to_E_F=True, slab_volume=pos.slab_volume())

# number = hbar^2/(2*m) * (3*pi^2)^(2/3) in eV * angstrom
E_F_free = 36.4645 * (pos.num_substrate_atoms() / pos.slab_volume())**(2.0/3.0)

plot_dos(dos, 0.0, E_F_free, outpath)
