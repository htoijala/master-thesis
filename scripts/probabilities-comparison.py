#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def read_arg_file(path):
    with open(path, 'r') as fd:
        lines = fd.readlines()

    data = {}

    linenum = 0
    status = 'label'

    prop_cycle = plt.rcParams['axes.prop_cycle']
    plot_params = [parset for parset in prop_cycle]

    label = ''
    for line in lines:
        linenum += 1
        L = line.strip()
        if L.startswith('#') or len(L) == 0:
            continue

        if L.startswith('['):
            status = 'label'

        if status == 'label':
            if not L.startswith('[') or not L.endswith(']'):
                raise RuntimeError('Malformed label on line {:d}.'.format(linenum))

            label = L[1:-1]
            data[label] = {}
            data[label]['plot_params'] = plot_params[(len(data.keys()) - 1) % len(plot_params)]

            status = 'files'

            continue

        if status == 'files':
            ind = L.find(' ')
            if ind == -1:
                raise RuntimeError('No space on line {:d}.'.format(linenum))

            try:
                field = float(L[:ind])
            except ValueError:
                raise RuntimeError('Malformed field strength on line {:d}.'.format(linenum))

            filepath = L[ind:].strip()
            if filepath.startswith('"') and filepath.endswith('"'):
                filepath = filepath[1:-1]

            try:
                E, D = np.loadtxt(filepath, skiprows=2, unpack=True)
            except IOError:
                raise RuntimeError('Unable to read file at {}.'.format(filepath))

            if not 'data' in data[label].keys():
                data[label]['data'] = {}

            if not field in data[label]['data'].keys():
                data[label]['data'][field] = (E, D)

            continue

    return data

def plot(data, outpath):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(r'Normal energy $E_{z} - E_{\text{F}} \unitsep \si{\electronvolt}$')
    ax.set_ylabel(r'Transmission probability $D(E_{z})$')

    ax.set_xlim(-3, 5)
    ax.grid(True)
    ax.set_yscale('log')

    in_legend = {}

    for L in sorted(data.keys()):
        in_legend[L] = False

        for field in sorted(data[L]['data'].keys()):
            E, D = data[L]['data'][field]
            ax.plot(E, D, label=(L if not in_legend[L] else None), linewidth=2, **data[L]['plot_params'])
            in_legend[L] = True

    ax.text(-0.5, 2e-39, r'\SI{1}{\giga\volt\per\meter}')
    ax.text(-2.0, 2e-9, r'\SI{3}{\giga\volt\per\meter}')

    ax.legend(loc='lower right')

    fig.tight_layout(pad=0.3)
    plt.savefig(outpath)
    plt.close()

outpath = sys.argv[1]
inpath = sys.argv[2]

data = read_arg_file(inpath)
plot(data, outpath)
