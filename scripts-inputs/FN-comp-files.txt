[Clean]
4.76484
-0.285919 0.992878
0.1 data/Cu-surface-clean/transmittances/0.1_curr.txt
0.2 data/Cu-surface-clean/transmittances/0.2_curr.txt
0.3 data/Cu-surface-clean/transmittances/0.3_curr.txt

[Clean Image]
4.76484
-0.555868 0.977663
0.1 data/Cu-surface-clean/image/transmittances/0.1_curr.txt
0.2 data/Cu-surface-clean/image/transmittances/0.2_curr.txt
0.3 data/Cu-surface-clean/image/transmittances/0.3_curr.txt

[Adatom]
4.44481
-0.166923 0.986091
0.1 /home/heiktoij/vasp/Cu-surface-adatom/transmittances/4_0.1_curr.txt
0.2 /home/heiktoij/vasp/Cu-surface-adatom/transmittances/4_0.2_curr.txt
0.3 /home/heiktoij/vasp/Cu-surface-adatom/transmittances/4_0.3_curr.txt

[Adatom Image]
4.44481
-0.456262 0.970922
0.1 /home/heiktoij/vasp/Cu-surface-adatom/image/transmittances/4_0.1_curr.txt
0.2 /home/heiktoij/vasp/Cu-surface-adatom/image/transmittances/4_0.2_curr.txt
0.3 /home/heiktoij/vasp/Cu-surface-adatom/image/transmittances/4_0.3_curr.txt
