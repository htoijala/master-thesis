MAKEFLAGS += --no-builtin-rules --no-builtin-variables

# define input files
FILE=main.tex
BIBFILES=sources.bib
FIGS= \
	SN-barrier.pdf SN-prob.pdf SN-curr.pdf SN-totcurr.pdf \
	conv-encut.pdf conv-kpt.pdf conv-sigma.pdf conv-lconst.pdf conv-work.pdf conv-layers.pdf \
	image-transition.pdf image-xc.pdf image-compare.pdf \
	potential-orig.pdf potential-processed.pdf undersampling.pdf \
	potential-2d-clean.pdf potential-2d-step.pdf potential-2d-adatom.pdf potential-2d-pyramid.pdf \
	potential-2d-all.pdf potential-2d-adatom-field1.pdf potential-2d-adatom-field2.pdf potential-2d-adatom-field3.pdf \
	workfunctions.pdf \
	probabilities.pdf dos.pdf currents.pdf \
	FN-plot.pdf FN-plot-cleanwork.pdf \
	probabilities-comparison.pdf FN-plot-comparison.pdf
STATIC_FIGS=\
	vaakuna.pdf \
	fcc111.tikz \
	geom-clean.png geom-step.png geom-adatom.png geom-pyramid.png\
	image-schematic.tikz

# define input and output directories
DATA_DIR=data
DATA_SCRIPT_DIR=$(DATA_DIR)/scripts

HEADER_DIR=header
MPL_DIR=header
SCRIPT_DIR=scripts
SCRIPT_INPUT_DIR=scripts-inputs
STATIC_FIG_DIR=figs
BUILD_DIR=build
FIG_BUILD_DIR=$(BUILD_DIR)/figs

# name of the latex output file
JOBNAME=$(basename $(FILE))
# name of the final output file
OUTNAME=Toijala_Heikki_Pro_gradu_2018

# define header files
HEADERS=packages.tex header.tex $(addprefix header-math-,$(addsuffix .tex,constants delimiter eqns modifiers set statistics vector)) cover.tex abstract.tex
MPL_FILES=matplotlib-packages.tex matplotlib.tex matplotlibrc

MPL=$(addprefix $(MPL_DIR)/,$(MPL_FILES))

# set bash as shell
export SHELL=bash

# how to run tex
TEX_INPUTS=$(shell pwd)/$(BUILD_DIR):$(shell pwd):$(shell pwd)/$(HEADER_DIR):
TEX_ENV=TEXINPUTS="$(TEX_INPUTS)"
TEX_ARG=--shell-escape --halt-on-error --interaction=batchmode --output-directory="$(BUILD_DIR)" --jobname="$(JOBNAME)" "$(FILE)"
TEX_EXEC=env $(TEX_ENV) lualatex $(TEX_ARG)
RUN_TEX=echo "Running lualatex" && $(TEX_EXEC) > /dev/null 2>&1

# how to run biber
BIB_INPUTS=$(shell pwd)/$(BUILD_DIR):$(shell pwd):
BIB_ENV=BIBINPUTS="$(BIB_INPUTS)"
BIB_ARG=--logfile "$(BUILD_DIR)/$(JOBNAME).blg" --outfile "$(BUILD_DIR)/$(JOBNAME).bbl" "$(BUILD_DIR)/$(JOBNAME).bcf"
BIB_EXEC=env $(BIB_ENV) biber $(BIB_ARG)
RUN_BIB=echo "Running biber" && $(BIB_EXEC) > /dev/null 2>&1

# how to run Python
PYTHON_ENV=$(TEX_ENV) PYTHONPATH="$(shell pwd)/$(SCRIPT_DIR):$(shell pwd)/$(DATA_SCRIPT_DIR):$(shell echo $$PYTHONPATH)" MATPLOTLIBRC="$(shell pwd)/$(MPL_DIR)"
PYTHON=python3
PYTHON_EXEC=$(PYTHON_ENV) $(PYTHON)
RUN_PYTHON=@echo "$(PYTHON)" "$(strip $(1))" "$(strip $(2))" && $(PYTHON_EXEC) $(strip $(1))

##################################################
# standard targets
##################################################

.PHONY: all
all: $(BUILD_DIR)/$(OUTNAME).pdf

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)/$(JOBNAME)*
	rm -rf $(BUILD_DIR)/document_inputs*
	rm -f $(BUILD_DIR)/$(OUTNAME).pdf

.PHONY: clean-all
clean-all:
	rm -rf $(BUILD_DIR)

##################################################
# data directory
##################################################

$(DATA_DIR):
	@echo "The $(DATA_DIR) directory should be a symlink to the directory containing the DFT and quantum transport data."
	@echo "The symlink must be created manually."
	@echo "Exiting with an error now."
	@return 1

##################################################
# build directories
##################################################

$(BUILD_DIR):
	mkdir $@

$(FIG_BUILD_DIR): | $(BUILD_DIR)
	mkdir $@

##################################################
# building the main pdf output file
##################################################

$(BUILD_DIR)/$(OUTNAME).pdf: $(BUILD_DIR)/$(JOBNAME).pdf data_inputs.tar.xz $(BUILD_DIR)/document_inputs.tar.xz
	pdftk $< attach_files $(filter-out $<,$^) output $@

$(BUILD_DIR)/$(JOBNAME).pdf: $(FILE) $(BIBFILES) $(addprefix $(FIG_BUILD_DIR)/,$(FIGS)) $(addprefix $(STATIC_FIG_DIR)/,$(STATIC_FIGS)) $(addprefix $(HEADER_DIR)/,$(HEADERS)) | $(BUILD_DIR)
	@\
	set -e ; \
	unset -v newest ; \
	for file in $(BIBFILES) ; do \
		[[ -z $$newest || $$file -nt $$newest ]] && newest=$$file ; \
	done ; \
	if [[ $@ -ot $$newest ]] ; then \
		force_bibrun=1 ; \
	else  \
		force_bibrun=0 ; \
	fi ; \
	$(RUN_TEX) ; \
	bibrun=1 ; \
	while [[ "$$(grep "Please (re)run Biber" $(BUILD_DIR)/$(JOBNAME).log)" != "" || $$force_bibrun != 0 ]] ; do \
		force_bibrun=0 ; \
		if [ $${bibrun} -gt 2 ] ; then \
			echo "Refusing to run biber more than 2 times." 1>&2 ; \
			echo "Your bibliography likely has a problem." 1>&2 ; \
			echo "A likely cause is a misspelled citation." 1>&2 ; \
			echo "See the logs for details." 1>&2 ; \
			exit 1 ; \
		fi ; \
		$(RUN_BIB) ; \
		$(RUN_TEX) ; \
		bibrun=$$(expr $${bibrun} + 1) ; \
	done ; \
	while grep "Package rerunfilecheck .* Rerun." $(BUILD_DIR)/$(JOBNAME).log > /dev/null ; do \
		$(RUN_TEX) ; \
	done

.PHONY: fast
fast: | $(BUILD_DIR)
	@$(RUN_TEX)

##################################################
# building the figures
##################################################

.PHONY: figures
figures: $(addprefix $(FIG_BUILD_DIR)/,$(FIGS))

# theory

$(FIG_BUILD_DIR)/SN-barrier.pdf: $(SCRIPT_DIR)/SN-barrier.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@

$(FIG_BUILD_DIR)/SN-prob.pdf: $(SCRIPT_DIR)/SN-prob.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@

$(FIG_BUILD_DIR)/SN-curr.pdf: $(SCRIPT_DIR)/SN-curr.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@

$(FIG_BUILD_DIR)/SN-totcurr.pdf: $(SCRIPT_DIR)/SN-totcurr.py $(SCRIPT_DIR)/FN_current.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@

# convergence tests

$(FIG_BUILD_DIR)/conv-encut.pdf: $(SCRIPT_DIR)/conv-encut.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-bulk/tetrahedron/build/results.txt

$(FIG_BUILD_DIR)/conv-kpt.pdf: $(SCRIPT_DIR)/conv-kpt.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-bulk/tetrahedron/build/results.txt

$(FIG_BUILD_DIR)/conv-sigma.pdf: $(SCRIPT_DIR)/conv-sigma.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-bulk/methfessel-paxton/build/results.txt

$(FIG_BUILD_DIR)/conv-lconst.pdf: $(SCRIPT_DIR)/conv-lconst.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-bulk/lattice_constant-fit/build/results.txt

$(FIG_BUILD_DIR)/conv-work.pdf: $(SCRIPT_DIR)/conv-layers.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/convergence.txt work

$(FIG_BUILD_DIR)/conv-layers.pdf: $(SCRIPT_DIR)/conv-layers.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/convergence.txt movement

# image potential

$(FIG_BUILD_DIR)/image-transition.pdf: $(SCRIPT_DIR)/image-transition.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@

$(FIG_BUILD_DIR)/image-xc.pdf: $(SCRIPT_DIR)/image-xc.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/nofield $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/field_0.2-har $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/field_0.2

$(FIG_BUILD_DIR)/image-compare.pdf: $(SCRIPT_DIR)/image-compare.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/field_0.2 $(DATA_DIR)/Cu-surface-clean/image/Cu_1uc_1uc_8ml_g15ml/field_0.2

# processing potential

$(FIG_BUILD_DIR)/potential-orig.pdf: $(SCRIPT_DIR)/potential-orig.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/field_0.2

$(FIG_BUILD_DIR)/potential-processed.pdf: $(SCRIPT_DIR)/potential-processed.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/field_0.2

# undersampling convergence

$(FIG_BUILD_DIR)/undersampling.pdf: $(SCRIPT_DIR)/undersampling.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/transmittances/convergence/0.1_4_prob.txt 4 $(DATA_DIR)/Cu-surface-clean/transmittances/convergence/0.1_6_prob.txt 6 $(DATA_DIR)/Cu-surface-clean/transmittances/convergence/0.1_8_prob.txt 8 $(DATA_DIR)/Cu-surface-clean/transmittances/convergence/0.1_10_prob.txt 10 $(DATA_DIR)/Cu-surface-clean/transmittances/convergence/0.1_12_prob.txt 12 $(DATA_DIR)/Cu-surface-clean/transmittances/convergence/0.1_20_prob.txt 20

# potential landscapes

$(FIG_BUILD_DIR)/potential-2d-clean.pdf: $(SCRIPT_DIR)/potential-2d.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/nofield x 8

$(FIG_BUILD_DIR)/potential-2d-step.pdf: $(SCRIPT_DIR)/potential-2d.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-step/Cu_1uc_8uc_8ml_g15ml_step/nofield y 1

$(FIG_BUILD_DIR)/potential-2d-adatom.pdf: $(SCRIPT_DIR)/potential-2d.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-adatom/Cu_4uc_2uc_8ml_g15ml_adatom/nofield x 3

$(FIG_BUILD_DIR)/potential-2d-pyramid.pdf: $(SCRIPT_DIR)/potential-2d.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-pyramid/Cu_4uc_2uc_8ml_g15ml_pyramid/nofield x 3

$(FIG_BUILD_DIR)/potential-2d-all.pdf: $(SCRIPT_DIR)/potential-2d-all.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/nofield y 8 "Clean surface" $(DATA_DIR)/Cu-surface-step/Cu_1uc_8uc_8ml_g15ml_step/nofield y 1 "Step" $(DATA_DIR)/Cu-surface-adatom/Cu_4uc_2uc_8ml_g15ml_adatom/nofield x 3 "Adatom" $(DATA_DIR)/Cu-surface-pyramid/Cu_4uc_2uc_8ml_g15ml_pyramid/nofield x 3 "Pyramid"

$(FIG_BUILD_DIR)/potential-2d-adatom-field1.pdf: $(SCRIPT_DIR)/potential-2d-field.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-adatom/image/Cu_4uc_2uc_8ml_g15ml_adatom/field_0.1 x 1

$(FIG_BUILD_DIR)/potential-2d-adatom-field2.pdf: $(SCRIPT_DIR)/potential-2d-field.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-adatom/image/Cu_4uc_2uc_8ml_g15ml_adatom/field_0.2 x 1

$(FIG_BUILD_DIR)/potential-2d-adatom-field3.pdf: $(SCRIPT_DIR)/potential-2d-field.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-adatom/image/Cu_4uc_2uc_8ml_g15ml_adatom/field_0.3 x 1

# work functions

$(FIG_BUILD_DIR)/workfunctions.pdf: $(SCRIPT_DIR)/workfunctions.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/nofield "Clean surface" $(DATA_DIR)/Cu-surface-adatom/Cu_4uc_2uc_8ml_g15ml_adatom/nofield "Adatom" $(DATA_DIR)/Cu-surface-step/Cu_1uc_8uc_8ml_g15ml_step/nofield "Step" $(DATA_DIR)/Cu-surface-pyramid/Cu_4uc_2uc_8ml_g15ml_pyramid/nofield "Pyramid"

# transmission probabilities

$(FIG_BUILD_DIR)/probabilities.pdf: $(SCRIPT_DIR)/probabilities.py $(SCRIPT_INPUT_DIR)/prob-files.txt $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(SCRIPT_INPUT_DIR)/prob-files.txt

# current densities

$(FIG_BUILD_DIR)/dos.pdf: $(SCRIPT_DIR)/dos.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/nofield/DOSCAR $(DATA_DIR)/Cu-surface-clean/Cu_1uc_1uc_8ml_g15ml/nofield/CONTCAR

$(FIG_BUILD_DIR)/currents.pdf: $(SCRIPT_DIR)/currents.py $(SCRIPT_INPUT_DIR)/curr-files.txt $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(SCRIPT_INPUT_DIR)/curr-files.txt

# FN plots

$(FIG_BUILD_DIR)/FN-plot.pdf: $(SCRIPT_DIR)/FN-plot.py $(SCRIPT_INPUT_DIR)/FN-files.txt $(SCRIPT_DIR)/FN_current.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(SCRIPT_INPUT_DIR)/FN-files.txt

$(FIG_BUILD_DIR)/FN-plot-cleanwork.pdf: $(SCRIPT_DIR)/FN-plot-cleanwork.py $(SCRIPT_INPUT_DIR)/FN-files.txt $(SCRIPT_DIR)/FN_current.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(SCRIPT_INPUT_DIR)/FN-files.txt

# PBE & image comparison

$(FIG_BUILD_DIR)/probabilities-comparison.pdf: $(SCRIPT_DIR)/probabilities-comparison.py $(SCRIPT_INPUT_DIR)/prob-comp-files.txt $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(SCRIPT_INPUT_DIR)/prob-comp-files.txt

$(FIG_BUILD_DIR)/FN-plot-comparison.pdf: $(SCRIPT_DIR)/FN-plot.py $(SCRIPT_INPUT_DIR)/FN-comp-files.txt $(SCRIPT_DIR)/FN_current.py $(MPL) | $(FIG_BUILD_DIR)
	$(call RUN_PYTHON, $<, $@) $@ $(SCRIPT_INPUT_DIR)/FN-comp-files.txt

##################################################
# building the document input archive
##################################################

$(BUILD_DIR)/document_inputs.tar.xz: $(HEADER_DIR) $(MPL_DIR) $(STATIC_FIG_DIR) $(SCRIPT_DIR) $(SCRIPT_INPUT_DIR) $(FILE) $(BIBFILES) Makefile
	$(eval DIRPATH := $(basename $(basename $@)))
	mkdir -p "$(DIRPATH)"
	rsync -aLk --delete-before --delete-excluded \
	      --exclude='*.pyc' \
	      --exclude='__pycache__' \
	      $^ "$(DIRPATH)/"
	cd $(BUILD_DIR) ; tar -I 'xz -9' -cf $(notdir $@) $(notdir $(DIRPATH))
